import requests
import os
import gzip
import shutil
from datetime import datetime

# URL and output file names
url = "http://localhost:5001/epg.xml.gz"
gz_file = "epg.xml.gz"
xml_file = "epg.xml"

def check_and_modify_xml(file_path):
    with open(file_path, 'r+', encoding='utf-8') as f:
        content = f.read()
        # Check if the XML declaration and DOCTYPE are present
        if not content.startswith('<?xml version="1.0" encoding="UTF-8"?>'):
            # Add the XML declaration and DOCTYPE
            f.seek(0, 0)
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            f.write('<!DOCTYPE tv SYSTEM "http://www.w3.org/2006/05/tv">\n')
            f.write(content)
            return True
        return False

try:
    # Sending a GET request to the URL
    response = requests.get(url, stream=True)
    response.raise_for_status()  # Raise an exception for bad responses (4xx or 5xx)

    # Saving the gzipped file to the local filesystem
    with open(gz_file, 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            f.write(chunk)

    print(f"Gzipped file downloaded successfully as '{gz_file}'.")

    # Extract the gzipped file
    with gzip.open(gz_file, 'rb') as f_in:
        with open(xml_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    print(f"File extracted successfully to '{xml_file}'.")

    # Check and modify the XML file if needed
    modified = check_and_modify_xml(xml_file)

    if modified:
        print(f"XML file modified successfully.")
        # Re-compress the modified XML file into the original gzipped file
        with open(xml_file, 'rb') as f_in:
            with gzip.open(gz_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        print(f"Modified XML file compressed successfully as '{gz_file}'.")

    # Remove the extracted XML file
    os.remove(xml_file)

    # Git commands to add, commit, and push
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M")
    os.system(f"git add {gz_file}")
    os.system(f"git commit -m 'Add modified epg at {current_time}'")
    os.system("git push -u origin main")

    print("Git commit and push completed.")

except requests.exceptions.RequestException as e:
    print(f"Failed to download the file: {e}")
except Exception as e:
    print(f"Error: {e}")
